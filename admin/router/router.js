let HomeController = require('./../controllers/HomeController');
let ConnexionController =  require('./../controllers/ConnexionController');
let VipsController =  require('./../controllers/VipsController');
let PhotosController =  require('./../controllers/PhotosController');

// Routes
module.exports = function(app){

// Main Routes
  app.get('/', HomeController.Index);
  app.get('/Home', HomeController.Index);

//Connexion
  app.post('/Connexion', ConnexionController.Connexion);
  app.get('/PageConnexion', ConnexionController.testConnexion);
  app.get('/Deconnexion', ConnexionController.Deconnexion);

//Vips
  app.get('/vips/Vips', ConnexionController.testConnexion, VipsController.vips);

    //Ajouter
    app.get('/vips/Vips/AjouterVipTest', ConnexionController.testConnexion, VipsController.AjouterVipTest);
    app.post('/vips/Vips/AjouterVip', ConnexionController.testConnexion, VipsController.AjouterVip);

    //Modifier
    app.get('/vips/Vips/ModifierVipTest', ConnexionController.testConnexion, VipsController.ModifierVipTest);
    app.post('/vips/Vips/ModifierVip', ConnexionController.testConnexion, VipsController.ModifierVip);

    //Supprimer
    app.get('/vips/Vips/SupprimerVipTest', ConnexionController.testConnexion, VipsController.SupprimerVipTest);
    app.post('/vips/Vips/SupprimerVip',  ConnexionController.testConnexion, VipsController.SupprimerVip);

//Photos
  app.get('/vips/Photos', ConnexionController.testConnexion, PhotosController.photos);

    //Ajouter
    app.get('/vips/Photos/AjouterPhotoTest', ConnexionController.testConnexion, PhotosController.AjouterPhotoTest);
    app.post('/vips/Photos/AjouterPhoto', ConnexionController.testConnexion, PhotosController.AjouterPhoto);

    //Supprimer
    app.get('/vips/Photos/SupprimerPhotoTest', ConnexionController.testConnexion, PhotosController.SupprimerPhotoTest);
    app.post('/vips/Photos/SupprimerPhoto', ConnexionController.testConnexion, PhotosController.SupprimerPhoto);
    app.post('/vips/Photos/SupprimerPhoto2/:vipNum', ConnexionController.testConnexion, PhotosController.SupprimerPhoto2);

// tout le reste
  app.get('*', HomeController.NotFound);
  app.post('*', HomeController.NotFound);

};
