let db = require('../configDb');


module.exports.getAuthentif = function(callback) {
  db.getConnection(function(err, connexion) {
      if (!err) {
          let sql = "SELECT * FROM PARAMETRES";
          // console.log(sql);
          connexion.query(sql, callback);
          connexion.release();
      }
  });
}


module.exports.nationalites = function(callback) {
  db.getConnection(function(err, connexion) {
      if (!err) {
          let sql = "SELECT * FROM NATIONALITE";
          // console.log(sql);
          connexion.query(sql, callback);
          connexion.release();
      }
  });
}

module.exports.vips = function(callback) {
  db.getConnection(function(err, connexion) {
    if (!err) {
      let sql = "SELECT * FROM VIP ORDER BY VIP_NOM";
      // console.log(sql);
      connexion.query(sql, callback);
      connexion.release();
    }
  });
}

module.exports.AddVip = function(vip, callback) {
  db.getConnection(function(err, connexion) {
      if (!err) {
          let sql = "INSERT INTO VIP SET NATIONALITE_NUMERO = " + vip.NATIONALITE_NUMERO + ", VIP_NOM = '" + vip.VIP_NOM + "', VIP_PRENOM = '" +  vip.VIP_PRENOM;
          sql = sql + "', VIP_SEXE = '" + vip.VIP_SEXE + "', VIP_NAISSANCE = '" + vip.VIP_NAISSANCE + "', VIP_TEXTE = '" + vip.VIP_TEXTE + "', VIP_DATE_INSERTION = NOW()";
          // console.log(sql);
          connexion.query(sql, callback);
          connexion.release();
      }
  });
}

module.exports.AddPhotoVip = function(photoVip, vipId, callback) {
  db.getConnection(function(err, connexion) {
      if (!err) {
          let sql = "INSERT INTO PHOTO SET PHOTO_NUMERO = '1', VIP_NUMERO = " + vipId + ", PHOTO_SUJET = '"+ photoVip.PHOTO_SUJET;
          sql = sql + "', PHOTO_COMMENTAIRE = '" + photoVip.PHOTO_COMMENTAIRE + "', PHOTO_ADRESSE = '" + photoVip.PHOTO_ADRESSE + "'";
          // console.log(sql);
          connexion.query(sql, callback);
          connexion.release();
      }
  });
}


module.exports.UpdateVip = function(vip, callback) {
  db.getConnection(function(err, connexion) {
      if (!err) {
          let sql = "UPDATE VIP SET NATIONALITE_NUMERO = " + vip.NATIONALITE_NUMERO + ", VIP_NOM = '" + vip.VIP_NOM + "', VIP_PRENOM = '" +  vip.VIP_PRENOM;
          sql = sql + "', VIP_SEXE = '" + vip.VIP_SEXE + "', VIP_NAISSANCE = '" + vip.VIP_NAISSANCE + "', VIP_TEXTE = '" + vip.VIP_TEXTE;
          sql = sql + "' WHERE VIP_NUMERO = " + vip.VIP_NUMERO;
          // console.log(sql);
          connexion.query(sql, callback);
          connexion.release();
      }
  });
}

module.exports.UpdateVipPhoto = function(photoVip, callback) {
  db.getConnection(function(err, connexion) {
      if (!err) {
          let sql = "UPDATE PHOTO SET PHOTO_NUMERO = 1, VIP_NUMERO = " + photoVip.VIP_NUMERO + ", PHOTO_SUJET = '"+ photoVip.PHOTO_SUJET;
          sql = sql + "', PHOTO_COMMENTAIRE = '" + photoVip.PHOTO_COMMENTAIRE + "', PHOTO_ADRESSE = '" + photoVip.PHOTO_ADRESSE;
          sql = sql + "' WHERE VIP_NUMERO = " + photoVip.VIP_NUMERO;
          // console.log(sql);
          connexion.query(sql, callback);
          connexion.release();
      }
  });
}

module.exports.DeleteVip = function(vidId, callback) {
    db.getConnection(function(err, connexion) {
        if (!err) {
            let sql = "DELETE FROM COMPORTE WHERE VIP_NUMERO = " + vidId + ";";
            sql = sql + "DELETE FROM PHOTO WHERE VIP_NUMERO = " + vidId + ";";
            sql = sql + "DELETE FROM DEFILEDANS WHERE VIP_NUMERO = " + vidId + ";";
            sql = sql + "DELETE FROM APOURAGENCE WHERE VIP_NUMERO = " + vidId + ";";
            sql = sql + "DELETE FROM MANNEQUIN WHERE VIP_NUMERO = " + vidId + ";";
            sql = sql + "DELETE FROM DEFILE WHERE VIP_NUMERO = " + vidId + ";";
            sql = sql + "DELETE FROM COUTURIER WHERE VIP_NUMERO = " + vidId + ";";
            sql = sql + "DELETE FROM COMPOSER WHERE VIP_NUMERO = " + vidId + ";";
            sql = sql + "DELETE FROM CHANTEUR WHERE VIP_NUMERO = " + vidId + ";";
            sql = sql + "DELETE FROM JOUE WHERE VIP_NUMERO = " + vidId + ";";
            sql = sql + "DELETE FROM FILM WHERE VIP_NUMERO = " + vidId + ";";
            sql = sql + "DELETE FROM ACTEUR WHERE VIP_NUMERO = " + vidId + ";";
            sql = sql + "DELETE FROM REALISATEUR WHERE VIP_NUMERO = " + vidId + ";";
            sql = sql + "DELETE FROM MARIAGE WHERE VIP_NUMERO = " + vidId + " OR VIP_VIP_NUMERO = " + vidId + ";";
            sql = sql + "DELETE FROM LIAISON WHERE VIP_NUMERO = " + vidId + " OR VIP_VIP_NUMERO = " + vidId + ";";
            sql = sql + "DELETE FROM APOURSUJET WHERE VIP_NUMERO = " + vidId + ";";
            sql = sql + "DELETE FROM VIP WHERE VIP_NUMERO = " + vidId + ";";
            console.log(sql);
            connexion.query(sql, callback);
            connexion.release();
        }
    });
  }

module.exports.DeleteVipPhoto = function(vipId, callback) {
  db.getConnection(function(err, connexion) {
      if (!err) {
          let sql = "DELETE FROM PHOTO WHERE VIP_NUMERO = " + vipId;
          // console.log(sql);
          connexion.query(sql, callback);
          connexion.release();
      }
  });
}


module.exports.photoNum = function(vipId, callback) {
  db.getConnection(function(err, connexion) {
      if (!err) {
          let sql = "SELECT MAX(PHOTO_NUMERO) AS photoNum FROM PHOTO WHERE VIP_NUMERO = " + vipId;
          // console.log(sql);
          connexion.query(sql, callback);
          connexion.release();
      }
  });
}

module.exports.photosVip = function(vipId, callback) {
  db.getConnection(function(err, connexion) {
    if (!err) {
      let sql = "SELECT * FROM PHOTO WHERE VIP_NUMERO = " + vipId + " AND PHOTO_NUMERO != 1";
      // console.log(sql);
      connexion.query(sql, callback);
      connexion.release();
    }
  });
}

module.exports.AddPhoto = function(photoNum, photoVip, callback) {
  db.getConnection(function(err, connexion) {
      if (!err) {
          let sql = "INSERT INTO PHOTO SET PHOTO_NUMERO = " + photoNum + ", VIP_NUMERO = " + photoVip.VIP_NUMERO + ", PHOTO_SUJET = '"+ photoVip.PHOTO_SUJET;
          sql = sql + "', PHOTO_COMMENTAIRE = '" + photoVip.PHOTO_COMMENTAIRE + "', PHOTO_ADRESSE = '" + photoVip.PHOTO_ADRESSE + "'";
          // console.log(sql);
          connexion.query(sql, callback);
          connexion.release();
      }
  });
}

module.exports.DeletePhoto = function(vipId, photoNum, callback) {
  db.getConnection(function(err, connexion) {
      if (!err) {
            let sql = "DELETE FROM PHOTO WHERE VIP_NUMERO = " + vipId + " AND PHOTO_NUMERO = " + photoNum;
          // console.log(sql);
          connexion.query(sql, callback);
          connexion.release();
      }
  });
}
