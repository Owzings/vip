
let model = require("../models/vip.js")
let async = require ("async");

// ///////////////////////// R E P E R T O I R E    D E S     S T A R S

module.exports.Repertoire = function(request, response){
   response.title = 'Répertoire des stars';

   model.premiere(function(err,results){
    if (err) {
        console.log(err);
        return;
    }
    response.nod = results;
    response.render('repertoireVips', response);
   });
} ;





//Listage des vip en fonction de la lettre
module.exports.infoVip = function(request, response){
    response.title = 'Nom des stars';
    let data = request.params.lettre;
    async.parallel([
        function(callback){
            model.premiere(function(err, results) {callback(null,results)});
        },
        function(callback){
            model.infoVip(data, function(err,results){callback(null,results)});
        }
    ],
    function (err, results){
        if (err) {
            console.log(err);
            return;

        }
        response.nod = results[0];
        response.infoVip = results[1];
        // console.log(results[0]);
        response.render('listerVip', response);
    });
} ;



//Affichage détail vip en cliquant sur son nom
module.exports.detailVip = function(request, response){
    response.title = 'détail Vip';
    let data = request.params.VIP_NUMERO;
    // let data1 = request.params.lettre;
    async.parallel([
        function(callback){
            model.premiere(function(err, results) {callback(null,results)});
        },
        function (callback) {
            model.getPhotoPrincipale(data, function (err, result) { callback(null, result) });
         },
        function(callback){
            model.detailVip(data,function(err,results){callback(null,results)});
        },
        function(callback){
            model.getActeur(data,function(err, result){callback(null,result)});
          },
          function(callback){
            model.getActeurFilm(data,function(err, result){callback(null,result)});
          },
          function(callback){
            model.getRealisateur(data,function(err, result){callback(null,result)});
          },
          function(callback){
            model.getMannequin(data,function(err, result){callback(null,result)});
          },
          function(callback){
            model.getMannequinDefile(data,function(err, result){callback(null,result)});
          },
          function(callback){
            model.getCouturier(data,function(err, result){callback(null,result)});
          },
          function(callback){
            model.getChanteur(data,function(err, result){callback(null,result)});
          },
          function(callback){
            model.getMariageVip(data,function(err, result){callback(null,result)});
          },
          function(callback){
            model.getMariageVipV2(data,function(err, result){callback(null,result)});
          },
          function(callback){
            model.getLiaisonVip(data,function(err, result){callback(null,result)});
          },
          function(callback){
            model.getLiaisonVipV2(data,function(err, result){callback(null,result)});
          },
          function (callback){
            model.photos(data,function(err, result){callback(null,result)});
          }
        // function (callback) {
        //     model.getPrincipauxFilms(data, function (err, result) { callback(null, result) });
        //  }
        // function(callback){
        //     module.infoVip = (data,function(err,results){callback(null,results)});
        // }
    ],
    function (err, results){
        if (err) {
            console.log(err);
            return;

        }
        response.nod = results[0];
        // response.photo = results[1];
        response.detailVip = results[2];
        // console.log(results[2]);
        // response.infoVip = results[3];
        // response.films = results[3];
        response.acteurVip = results[3];
        response.acteurVipFilm = results[4];
        response.realisateurVip = results[5];
        response.mannequinVip = results[6];
        response.mannequinVipDefile = results[7];
        response.couturierVip = results[8];
        response.chanteurVip = results[9];
        response.mariagesVip = results[10];
        response.mariagesVipV2 = results[12];
        response.liaisonsVip = results[12];
        response.liaisonsVipV2 = results[13];
        response.photo = results[14];
        response.render('detailVip', response);
    });
};

module.exports.DetailLettre = function(request, response){
  response.title = 'Liste des Vip (noms commencent par la même lettre)';
  let lettre = request.params.vipNum;
  async.parallel([
    function(callback){
      model.getPremiereLettre(function(err, result) {callback(null, result)});
    },
    function(callback){
      model.getVipLettre(lettre, function(err2, result2){callback(null, result2)});
    }
  ],

  function(err, result){
    if(err){
      console.log(err);
      return
    }
    response.vipNum = result[0];
    response.listeVip = result[1];

    response.render('repertoireVipLettre', response);
  }

  );
}
