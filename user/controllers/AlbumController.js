let model = require("../models/vip.js");
let async = require("async");

// ////////////////////// L I S T E R     A L B U M S

module.exports.ListerAlbum = 	function(request, response){
   response.title = 'Album des stars';
   let data = request.params.VIP_NUMERO;
   //datai = request.params.nbVips;

   async.parallel([
     function(callback){
       model.listerAlbum(function(err, results){callback(null, results);});
     },
     function(callback){
       model.afficherAlbum(data, function(err, results){callback(null, results);});
     },
     function(callback){
       model.compterVips(function(err, results){callback(null, results);});
     }
   ],
   function(err, results){
     if(err){
       console.log(err);
       return;
     }
     response.albs = results[0];
     response.alb = results[1];
     response.nbVips = results[2];
     response.render('listerAlbum', response);
   });
};

module.exports.AfficherAlbum = function(request, response){
  response.title = "Album d'une star";
  let data = request.params.VIP_NUMERO;
  //datai = request.params.nbVips;


  async.parallel([
    function(callback){
      model.listerAlbum(function(err, results){callback(null, results);});
    },
    function(callback){
      model.afficherAlbum(data, function(err, results){callback(null, results);});
    },
    function(callback){
      model.compterVips(function(err, results){callback(null, results);});
    }
  ],
  function(err, results){
    if(err){
      console.log(err);
      return;
    }
    response.albs = results[0];
    response.alb = results[1];
    response.nbVips = results[2];
    response.render('listerAlbum', response);
  });
};

module.exports.CompterVips = function(request, response){
  response.title = "Nombre de vips";
  let data = request.params.VIP_NUMERO;
  //datai = request.params.nbVips;

  async.parallel([
    function(callback){
      model.listerAlbum(function(err, results){callback(null, results);});
    },
    function(callback){
      model.afficherAlbum(data, function(err, results){callback(null, results);});
    },
    function(callback){
      model.compterVips(function(err, results){callback(null, results);});
    }
  ],
  function(err, results){
    if(err){
      console.log(err);
      return;
    }
    response.albs = results[0];
    response.alb = results[1];
    response.nbVips = results[2];
    response.render('listerAlbum', response);
  });
};

//Afficher album
//Lister album
