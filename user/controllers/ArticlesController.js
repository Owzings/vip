let model = require("../models/vip.js")
let async = require("async");

// ///////////////////////// REP DES ARTICLES

module.exports.repArticles = function(request, response){
  response.title = 'Choix de star';

  model.vips(function(err, results){
    if(err){
      console.log(err);
      return;
    }
    response.vipsE = results;
    response.render('articles', response);
  });
};

//S'occupe de l'asynchronicité du menu des star et des articles
module.exports.ListerVip = function(request, response){
  let data = request.params.VIP_NUMERO;
  response.title = 'Choix star';
  async.parallel([
    function(callback){
      model.vips(function(err, results){callback(null, results)});
    },
    function(callback){
      model.articles(data, function(err, results){callback(null, results)});
    }
  ],
  function(err, results){
    if(err){
      console.log(err);
      return;
    }
    response.vipsE = results[0];
    response.articles = results[1];
    //console.log(results[1]);
    response.render("articles", response);
  });
};


//Permet théoriquement d'envoyer les données des articles d'une star
module.exports.ListerArticles = function(request, response){
  let data = request.params.VIP_NUMERO;
  response.title = 'Listage articles';
  async.parallel([
    function(callback){
      model.vips(function(err, results){callback(null, results)});
    },
    function(callback){
      model.articles(data, function(err, results){callback(null, results)});
    }
  ],
  function(err, results){
    if(err){
      console.log(err);
      return;
    }
    response.vipsE = results[0];
    response.articles = results[1];
    console.log(results[1]);
    response.render("ListerArticles", response);
  });
};
