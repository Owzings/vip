let db = require('../configDb');


module.exports.test = function(callback) {
    db.getConnection(function(err, connexion) {
        if (!err) {
            let sql = "SELECT COUNT(*) AS NB FROM vip ;";
              // console.log(sql);
            connexion.query(sql, callback);
            connexion.release();
        }
    });

};



//Requete qui affiche la premiere lettre des noms des vip
module.exports.premiere = function(callback) {
    db.getConnection(function(err, connexion) {
        if (!err) {
            let sql = "SELECT distinct SUBSTRING(VIP_NOM,1,1) AS LETTRE FROM vip order by LETTRE ASC";
            // console.log(sql);
            connexion.query(sql, callback);
            connexion.release();
        }
    });

};

//requete qui donne photo, nom, prenom et num du vip sur qui on a cliqué
module.exports.infoVip = function(data, callback) {
    db.getConnection( function(err,  connexion) {
        if (!err) {
            let sql = "SELECT vip.VIP_NUMERO, PHOTO_ADRESSE, VIP_NOM, VIP_PRENOM FROM vip inner join photo on vip.VIP_NUMERO=photo.VIP_NUMERO where PHOTO_NUMERO = 1 and SUBSTRING(VIP_NOM,1,1) = '"+data+"'";
             // console.log(sql);
            connexion.query(sql, callback);
            connexion.release();
        }
    });

};


module.exports.detailVip = function(data, callback) {
    db.getConnection( function(err,  connexion) {
        if (!err) {
            let sql = "SELECT NATIONALITE_NOM,VIP_NOM,VIP_PRENOM, VIP_SEXE, VIP_NAISSANCE as dateNaissance, VIP_TEXTE from vip inner join nationalite on vip.NATIONALITE_NUMERO=nationalite.NATIONALITE_NUMERO where VIP_NUMERO='"+data+"'";
            // console.log(sql);
            connexion.query(sql, callback);
            connexion.release();
        }
    });
};

//Requete qui retourne la photo principale du vip
module.exports.getPhotoPrincipale = function (id,callback) {
    db.getConnection(function (err, connexion) {
        if (!err) {
            let sql = "SELECT p.PHOTO_ADRESSE as photo from vip v JOIN photo p on p.vip_numero = v.vip_numero where v.vip_numero  = '"+id+"' and photo_numero = 1";
             //console.log(sql);
            connexion.query(sql, callback);
            connexion.release();
        }
    });
};

module.exports.getActeur = function(data,callback) {
  db.getConnection(function(err, connexion) {
    if(!err){
      let sql = "SELECT http://a.VIP _NUMERO, VIP_SEXE FROM acteur a JOIN vip v ON http://v.VIP _NUMERO=https://t.co/SpKoR6WA77_NUMERO WHERE http://a.VIP _NUMERO="+data+";";
      //console.log(sql);
      connexion.query(sql, callback); connexion.release();
    }
  });
};

module.exports.getActeurFilm = function(data,callback) {
  db.getConnection(function(err, connexion) {
    if (!err){
      let sql = "SELECT http://j.VIP _NUMERO, FILM_TITRE, FILM_DATEREALISATION ,http://r.VIP _NUMERO as realNum, http://v.VIP _NOM, http://v.VIP _PRENOM FROM acteur a ";
      sql = sql + "JOIN joue j ON http://a.VIP _NUMERO=https://t.co/Lwi6ZLnskg_NUMERO JOIN film f ON http://j.FILM _NUMERO=https://t.co/nAu5weinfe_NUMERO LEFT JOIN realisateur r ";
      sql = sql + "ON http://f.VIP _NUMERO=https://t.co/N2I0po4DSm_NUMERO LEFT JOIN vip v ON http://r.VIP _NUMERO=https://t.co/dzIJSLnlkg_NUMERO WHERE http://j.VIP _NUMERO="+data+";";
      //console.log(sql);
      connexion.query(sql, callback);
      connexion.release();
    }
  });
};

module.exports.getRealisateur = function(data,callback) {
  db.getConnection(function(err, connexion) {
    if (!err) { let sql = "SELECT http://r.VIP _NUMERO, FILM_TITRE, FILM_DATEREALISATION, VIP_SEXE FROM realisateur r LEFT JOIN film f ON http://r.VIP _NUMERO=https://t.co/CEjif3wRAU_NUMERO JOIN vip v ON http://r.VIP _NUMERO=https://t.co/dzIJSLnlkg_NUMERO WHERE http://r.VIP _NUMERO="+data+";";
      //console.log(sql);
      connexion.query(sql, callback);
      connexion.release();
    }
  });
};
module.exports.getMannequin = function(data,callback) {
  db.getConnection(function(err, connexion) {
    if (!err) {
      let sql = "SELECT http://m.VIP _NUMERO, VIP_SEXE FROM mannequin m JOIN vip v ON http://v.VIP _NUMERO=https://t.co/vEue9lhsX2_NUMERO WHERE http://m.VIP _NUMERO="+data+";";
      //console.log(sql);
      connexion.query(sql, callback);
      connexion.release();
    }
  });
};
module.exports.getMannequinDefile = function(data,callback) {
  db.getConnection(function(err, connexion) {
    if (!err) {
      let sql = "SELECT http://dd.VIP _NUMERO, DEFILE_LIEU, DEFILE_DATE ,http://c.VIP _NUMERO coutNum, http://v.VIP _NOM, http://v.VIP _PRENOM FROM mannequin m ";
      sql = sql + "JOIN defiledans dd ON http://m.VIP _NUMERO=https://t.co/ZoxiKEguLg_NUMERO JOIN defile d ON dd.DEFILE_NUMERO=d.DEFILE_NUMERO ";
      sql = sql + "LEFT JOIN couturier c ON http://d.VIP _NUMERO=https://t.co/etzBM06KSk_NUMERO LEFT JOIN vip v ON http://c.VIP _NUMERO=https://t.co/dzIJSLnlkg_NUMERO WHERE http://dd.VIP _NUMERO="+data+";";
      //console.log(sql);
      connexion.query(sql, callback);
      connexion.release();
    }
  });
};
module.exports.getCouturier = function(data,callback) {
  db.getConnection(function(err, connexion) {
    if (!err) {
      let sql = "SELECT http://c.VIP _NUMERO, DEFILE_LIEU, DEFILE_DATE, VIP_SEXE FROM couturier c LEFT JOIN defile d ON http://c.VIP _NUMERO=https://t.co/2DUXluV0PE_NUMERO JOIN vip v ON http://c.VIP _NUMERO=https://t.co/dzIJSLnlkg_NUMERO WHERE http://c.VIP _NUMERO="+data+";";
      //console.log(sql);
      connexion.query(sql, callback);
      connexion.release();
    }
  });
};
module.exports.getChanteur = function(data,callback) {
  db.getConnection(function(err, connexion) {
    if (!err) {
      let sql = "SELECT http://c.VIP _NUMERO, ALBUM_TITRE, ALBUM_DATE, CHANTEUR_SPECIALITE, MAISONDISQUE_NOM FROM chanteur c JOIN composer co ON ";
      sql = sql + "http://c.VIP _NUMERO=https://t.co/u06vjnfc9W_NUMERO JOIN album a ON co.ALBUM_NUMERO=a.ALBUM_NUMERO JOIN maisondisque m ON ";
      sql = sql + "a.MAISONDISQUE_NUMERO=m.MAISONDISQUE_NUMERO WHERE http://c.VIP _NUMERO="+data+";";
      //console.log(sql);
      connexion.query(sql, callback);
      connexion.release();
    }
  });
};
module.exports.getMariageVip = function(data,callback) {
  db.getConnection(function(err, connexion) {
    if (!err) {
      let sql = "SELECT VIP_VIP_NUMERO, VIP_NOM, VIP_PRENOM, DATE_EVENEMENT, MARIAGE_FIN, MARIAGE_LIEU, MARIAGE_MOTIFFIN FROM mariage m JOIN vip v ON ";
      sql = sql + "http://m.VIP _VIP_NUMERO=https://t.co/dzIJSLnlkg_NUMERO WHERE http://m.VIP _NUMERO="+data+";";
      //console.log(sql);
      connexion.query(sql, callback);
      connexion.release();
    }
  });
};
module.exports.getMariageVipV2 = function(data,callback) {
  db.getConnection(function(err, connexion) {
    if (!err) {
      let sql = "SELECT http://m.VIP _NUMERO, VIP_NOM, VIP_PRENOM, DATE_EVENEMENT, MARIAGE_LIEU, MARIAGE_FIN, MARIAGE_MOTIFFIN FROM mariage m JOIN vip v ON ";
      sql = sql + "http://m.VIP _NUMERO=https://t.co/dzIJSLnlkg_NUMERO WHERE http://m.VIP _VIP_NUMERO="+data+";";
      //console.log(sql);
      connexion.query(sql, callback);
      connexion.release();
    }
  });
};
module.exports.getLiaisonVip = function(data,callback) {
  db.getConnection(function(err, connexion) {
    if (!err) {
      let sql = "SELECT VIP_VIP_NUMERO, VIP_NOM, VIP_PRENOM, DATE_EVENEMENT, LIAISON_MOTIFFIN FROM liaison l JOIN vip v ON ";
      sql = sql + "http://l.VIP _VIP_NUMERO=https://t.co/dzIJSLnlkg_NUMERO WHERE http://l.VIP _NUMERO="+data+";";
      //console.log(sql);
      connexion.query(sql, callback);
      connexion.release();
    }
  });
};
module.exports.getLiaisonVipV2 = function(data,callback) {
  db.getConnection(function(err, connexion) {
    if (!err) {
      let sql = "SELECT http://l.VIP _NUMERO, VIP_NOM, VIP_PRENOM, DATE_EVENEMENT, LIAISON_MOTIFFIN FROM liaison l JOIN vip v ON ";
      sql = sql + "http://l.VIP _NUMERO=https://t.co/dzIJSLnlkg_NUMERO WHERE http://l.VIP _VIP_NUMERO="+data+";";
      //console.log(sql);
      connexion.query(sql, callback);
      connexion.release();
    }
  });
};
module.exports.photos = function(data,callback) {
    db.getConnection(function(err, connexion) {
        if (!err) {
            let sql = "SELECT PHOTO_ADRESSE, PHOTO_NUMERO, PHOTO_SUJET, PHOTO_COMMENTAIRE FROM photo WHERE VIP_NUMERO=" +data+ ";";
                          //console.log(sql);
            connexion.query(sql, callback);
            connexion.release();
        }
    });
};


//-----------------ALBUM PHOTO----------------
module.exports.listerAlbum = function(callback){
  db.getConnection(function(err, connexion){
    if(!err){
      let sql = "SELECT v.VIP_NUMERO, PHOTO_ADRESSE FROM PHOTO p JOIN VIP v ON v.VIP_NUMERO=p.VIP_NUMERO WHERE PHOTO_NUMERO = 1 ORDER BY VIP_PRENOM, VIP_NOM";
      console.log(sql);
      connexion.query(sql, callback);
      connexion.release();
    }
  });
};

module.exports.afficherAlbum = function(data, callback){
  db.getConnection(function(err, connexion){
    if(!err){
      let sql = "SELECT v.VIP_NUMERO, VIP_NOM, VIP_PRENOM, PHOTO_ADRESSE, PHOTO_COMMENTAIRE FROM PHOTO p JOIN VIP v ON v.VIP_NUMERO=p.VIP_NUMERO WHERE v.VIP_NUMERO="+data;
      console.log(sql);
      connexion.query(sql, callback);
      connexion.release();
    }
  });
};

module.exports.compterVips = function(callback){
  db.getConnection(function(err, connexion){
    if(!err){
      let sql = "SELECT COUNT(*) as nbVips FROM VIP";
      console.log(sql);
      connexion.query(sql, callback);
      connexion.release();
    }
  });
};

//------------------ARTICLES------------------
//Sert a lister les VIPS pour la page articles
module.exports.vips = function(callback){
  db.getConnection(function(err, connexion){
    if(!err){
      let sql = "SELECT VIP_NUMERO, VIP_PRENOM, VIP_NOM FROM VIP ORDER BY VIP_NOM, VIP_PRENOM";
      connexion.query(sql, callback);
      connexion.release();
    }
  });
};

//Liste les articles d'une star
module.exports.articles = function(data, callback){
  db.getConnection(function(err, connexion){
    if(!err){
      let sql = "SELECT v.VIP_NUMERO ,VIP_PRENOM, VIP_NOM, ARTICLE_RESUME, ARTICLE_DATE_INSERT FROM VIP v JOIN APOURSUJET aps ON v.VIP_NUMERO=aps.VIP_NUMERO JOIN ARTICLE a ON aps.ARTICLE_NUMERO=a.ARTICLE_NUMERO WHERE v.VIP_NUMERO="+data;
      connexion.query(sql, callback);
      connexion.release();
    }
  });
};
